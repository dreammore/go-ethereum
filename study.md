# 初始化链
--datadir D:\WORKSPACE\180\test\data --networkid 200 init D:\WORKSPACE\180\test\init.json

# 启动私链
--datadir D:\WORKSPACE\180\test\data --networkid 200 --nodiscover console --http --http.addr 0.0.0.0 --http.port  8545 --http.api "eth,net,web3,personal,admin,miner" --allow-insecure-unlock --rpc.allow-unprotected-txs

geth.exe --datadir data init init.json
geth.exe --datadir data --networkid 1000 --nodiscover console --http --http.addr 0.0.0.0 --http.port  8545 --http.api "eth,net,web3,personal,admin,miner" --allow-insecure-unlock --rpc.allow-unprotected-txs


# 查看余额
eth.getBalance(eth.accounts[0])

eth.getBalance(eth.accounts[1])

eth.getBalance(eth.accounts[2])


# 解锁账号
personal.unlockAccount(eth.accounts[0])


var amount = web3.toWei(1,'ether')

# 发起转账
eth.sendTransaction({from:eth.accounts[0],to:eth.accounts[1],value:web3.toWei(1,'ether')})

eth.sendTransaction({from:eth.accounts[0],to:"28010eb02049168b1a9fe671181c0e485d5c51c9",value:amount})


eth.sendTransaction({from:eth.accounts[0],to:"0x464f490d0bfddf38cb6d98e70295599a6470c13d",value:amount})

eth.sendTransaction({from:"0x25e63ae307a51c70493d29ddf145c2d7113fa943",to:from:eth.accounts[0],value:amount})

eth.getBalance("0x464f490d0bfddf38cb6d98e70295599a6470c13d")


personal.unlockAccount(eth.accounts[1])


eth.sendTransaction({from:eth.accounts[1],to:eth.accounts[2],value:amount})

eth.getBlockByNumber(2)

eth.getTransaction("0x94fb78105df376ca40cdc715ce891cc6ab961b43ceb40fdb20d4beeafdf47dc8")


# 挖一个区块
miner.start(1);admin.sleepBlocks(1);miner.stop();

# 查看未确认交易
txpool.status
# 显示
{
  pending: 1,
  queued: 0
}

# 等待确认交易
eth.getBlock("pending", true).transactions




func GetAPIs(apiBackend Backend) []rpc.API {
	nonceLock := new(AddrLocker)
	return []rpc.API{
		{
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicEthereumAPI(apiBackend),
			Public:    true,
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicBlockChainAPI(apiBackend),
			Public:    true,
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicTransactionPoolAPI(apiBackend, nonceLock),
			Public:    true,
		}, {
			Namespace: "txpool",
			Version:   "1.0",
			Service:   NewPublicTxPoolAPI(apiBackend),
			Public:    true,
		}, {
			Namespace: "debug",
			Version:   "1.0",
			Service:   NewPublicDebugAPI(apiBackend),
			Public:    true,
		}, {
			Namespace: "debug",
			Version:   "1.0",
			Service:   NewPrivateDebugAPI(apiBackend),
		}, {
			Namespace: "eth",
			Version:   "1.0",
			Service:   NewPublicAccountAPI(apiBackend.AccountManager()),
			Public:    true,
		}, {
			Namespace: "personal",
			Version:   "1.0",
			Service:   NewPrivateAccountAPI(apiBackend, nonceLock),
			Public:    false,
		},
	}
}



### only replay-protected (EIP-155) transactions allowed over RPC
两个方案可以解决
1、如果是自己的节点，同步命令加上 --rpc.allow-unprotected-txs

2、如果是第三方的节点，签名内容增加chainId

let chainID =await web3.eth.getChainId();
//
let rawTransaction = {
    from: fromAddr,
    to: to,
    value: value,
    gasLimit: toHex(gasLimit),
    gasPrice: toHex(gasPrice),
    data: data,
    chainId: toHex(chainID),//签名内容增加chainId
    nonce: nonce
};

let chainID =await web3.eth.getChainId();
//
let rawTransaction = {
    from: fromAddr,
    to: to,
    value: value,
    gasLimit: toHex(gasLimit),
    gasPrice: toHex(gasPrice),
    data: data,
    chainId: toHex(chainID),//签名内容增加chainId
    nonce: nonce
};