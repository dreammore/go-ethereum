package smcrypto

import (
	"github.com/ethereum/go-ethereum/sm2"
	"io"
	"math/big"
)

var (
	sm2H                 = new(big.Int).SetInt64(1)
	sm2SignDefaultUserId = []byte{
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38,
		0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38}
)

func GenerateKey(rand io.Reader) (*sm2.PrivateKey, *sm2.PublicKey, error) {
	priv, pub, err := sm2.GenerateKey(rand)
	if err != nil {
		return nil, nil, err
	}
	return priv, pub, nil
}

func P256() sm2.P256V1Curve  {
	return sm2.GetSm2P256V1()
}
