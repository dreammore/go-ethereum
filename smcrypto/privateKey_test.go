package smcrypto

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"github.com/ethereum/go-ethereum/sm2"
	"math/big"
	"testing"
)

func TestPrivateKey(t *testing.T) {
	priv, _, _ := GenerateKey(rand.Reader)
	privdata := Serialize(priv)

	priv2 := PrivKeyFromBytes(sm2.GetSm2P256V1(), privdata)

	fmt.Println(new(big.Int).SetBytes(privdata))
	fmt.Println(hex.EncodeToString(privdata))

	fmt.Println(priv.D)
	fmt.Println(priv2.D)
	pub := sm2.CalculatePubKey(priv)
	fmt.Println(hex.EncodeToString(SerializeUncompressed(pub)))



}
