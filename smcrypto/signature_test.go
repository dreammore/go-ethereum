package smcrypto

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"testing"
)

func TestMarshalSign(t *testing.T) {
	priv, _, _ := GenerateKey(rand.Reader)
	sign, _ := SignToSign(priv, []byte("happy"))
	fmt.Println(sign.R, sign.S, sign.V)

	data, _ := MarshalSign(sign.R, sign.S, sign.V)

	r, s, v, _ := UnmarshalSign(data)
	fmt.Println(r, s, v)

}

func TestParsePublicKey(t *testing.T) {
	priv, _, _ := GenerateKey(rand.Reader)
	sign, _ := SignToSign(priv, []byte("happy"))

	pub, _ := ParsePublicKey(priv.Curve, sign, []byte("happy"))
	fmt.Println(GetPublicKey(priv))
	fmt.Println(pub)

	signdata, _ := SignToData(priv, []byte("adopapa"))
	fmt.Println(len(signdata))

	rbytes := sign.R.Bytes()
	sbytes := sign.S.Bytes()
	vbytes := sign.V.Bytes()
	sig := make([]byte, 96)
	copy(sig[32-len(rbytes):32], rbytes)
	copy(sig[64-len(sbytes):64], sbytes)
	copy(sig[96-len(vbytes):96], vbytes)

	r := new(big.Int).SetBytes(sig[:32])
	s := new(big.Int).SetBytes(sig[32:64])
	v := new(big.Int).SetBytes(sig[64:96])

	fmt.Println(r, s, v)
	fmt.Println(sign.R, sign.S, sign.V)




}
