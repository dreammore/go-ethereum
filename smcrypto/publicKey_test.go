package smcrypto

import (
	"bytes"
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"testing"
)

func TestDecompressPublickey(t *testing.T) {
	_, pub, _ := GenerateKey(rand.Reader)
	fmt.Println(pub)
	fmt.Println(hex.EncodeToString(pub.GetUnCompressBytes()))
	pubdata := CompressPublickey(pub)
	pub2, _ := DecompressPublickey(pubdata)
	fmt.Println(pub2)

	pubdata2 := Compress(pub)
	pub3, _ := DecompressPublickey(pubdata2)
	fmt.Println(pub3)

	fmt.Println(bytes.Compare(pubdata, pubdata2))

}
