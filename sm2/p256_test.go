package sm2

import (
	"fmt"
	"math/big"
	"testing"
)

func TestP256Sm2(t *testing.T) {
	z := new(big.Int)
	fmt.Println(z)
}

func TestSm2P256(t *testing.T) {
	//input := "37107287533902102798797998220837590246510135740250"
	input := "100000000000000000000"
	a := big.NewInt(0)
	a.SetString(input, 10)
	b := Sm2P256Sqrt(a)

	fmt.Printf("%s : %v\n", input, b)
	fmt.Println(b)


}
